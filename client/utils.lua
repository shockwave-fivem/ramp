function GetGroundCoordsFromEntityOffset(entity, offX, offY, offZ)
  local pos = GetOffsetFromEntityInWorldCoords(entity, offX, offY, offZ)

  -- First, try to find the closest ground within 10 units.
  -- TODO: Line of sight must be clear. Sometimes colissions extend further
  -- underground, in which case the ramp can spawn wrongly.
  -- See https://streamable.com/ohns7
  for z=0,10 do
    local hit, ground = GetGroundZFor_3dCoord(pos.x, pos.y, pos.z + z)
    if hit then
      return vector3(pos.x, pos.y, ground)
    end
  end

  -- If that can't find the ground, just get the highest possible ground.
  local hit, ground = GetGroundZFor_3dCoord(pos.x, pos.y, pos.z + 1000)
  return vector3(pos.x, pos.y, ground)
end

function GetEntityForwardSpeed(entity)
  local speed = GetEntitySpeedVector(entity, true)
  return speed.y
end

function GetEntityRampCoords(entity)
  local speed = GetEntityForwardSpeed(entity)
  local offset = math.max(speed, 5.5)

  return GetGroundCoordsFromEntityOffset(entity, 0, offset, 0)
end

function LoadModel(model)
  local hash = GetHashKey(model)
  repeat
    RequestModel(hash)
    Citizen.Wait(0)
  until HasModelLoaded(hash)
  return hash
end

function SpawnObject(model, x, y, z)
  local hash = LoadModel(model)
  return CreateObjectNoOffset(model, x, y, z, false, true, false)
end

function CreateRamp(pos, angle)
  local model = 'stt_prop_ramp_jump_xs'
  local ramp = SpawnObject(model, pos.x, pos.y, pos.z)

  local min, max = GetModelDimensions(model)
  local offset = math.abs(min.x)

  SetEntityHeading(ramp, angle + 90)
  PlaceObjectOnGroundProperly(ramp)


  local new = GetOffsetFromEntityInWorldCoords(ramp, offset, 0, 0)
  SetEntityCoords(ramp, new.x, new.y, new.z - 0.1)
  FreezeEntityPosition(ramp, true)

  return ramp
end
