local INPUT_VEH_HORN = 86
local INPUT_SPECIAL_ABILITY_PC = 171

local syncedRamps = {}
local localRamp = nil

local function IsRampControlJustPressed()
  if not IsInputDisabled(2) then
    return IsControlJustPressed(2, INPUT_VEH_HORN)
  end

  return IsControlJustPressed(0, INPUT_SPECIAL_ABILITY_PC)
end

--------------------------------------------------------------------------------

RegisterNetEvent('ramp:create')
AddEventHandler('ramp:create', function (playerId, pos, angle)
  local pos = vector3(pos.x, pos.y, pos.z)

  if syncedRamps[playerId] then
    DeleteEntity(syncedRamps[playerId])
    syncedRamps[playerId] = nil
  end

  syncedRamps[playerId] = CreateRamp(pos, angle)
end)

--------------------------------------------------------------------------------

RegisterNetEvent('ramp:destroy')
AddEventHandler('ramp:destroy', function (playerId)
  if syncedRamps[playerId] then
    DeleteEntity(syncedRamps[playerId])
    syncedRamps[playerId] = nil
  end
end)

--------------------------------------------------------------------------------

AddEventHandler('onResourceStop', function (resourceName)
  if resourceName ~= GetCurrentResourceName() then
    return
  end

  if localRamp then
    DeleteEntity(localRamp)
  end

  for _, syncedRamp in pairs(syncedRamps) do
    DeleteEntity(syncedRamp)
  end
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function CheckControls()
    if not IsRampControlJustPressed() then
      return
    end

    local playerPed = PlayerPedId()
    local vehicle = GetVehiclePedIsIn(playerPed)

    if not DoesEntityExist(vehicle) then
      return
    end

    if localRamp then
      DeleteEntity(localRamp)
    end

    local pos = GetEntityRampCoords(vehicle)
    local angle = GetEntityHeading(vehicle)

    localRamp = CreateRamp(pos, angle)
    TriggerServerEvent('ramp:sync', { x=pos.x, y=pos.y, z=pos.z }, angle)
  end

  while true do
    Citizen.Wait(0)
    CheckControls()
  end
end)
